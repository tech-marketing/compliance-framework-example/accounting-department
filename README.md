# Accounting and Billing Department

### **Note: Do not edit this project directly, but rather clone it and configure it within your own GitLab instance.**

The accounting department examines what subscription plans users have and how much they are spending. It uses the data in `clients.json` to perform the following:

- Calculate Total Yearly Income
- Calculate Total Monthly Income
- Generate a bar chart on Yearly Income per Client

This project is used to showcase [separation of duties](https://csrc.nist.gov/glossary/term/separation_of_duty) using [GitLab Compliance Frameworks](https://docs.gitlab.com/ee/user/group/compliance_frameworks.html). You can see how to leverage compliance frameworks using this project by reading the documentation provided in the [Compliance Configurations](https://gitlab.com/gitlab-de/tutorials/security-and-governance/compliance-frameworks/compliance-configurations) project.

---

Created and maintained by [Fern](https://gitlab.com/fjdiaz)🌿

- [LinkedIn](https://www.linkedin.com/in/awkwardferny/)
- [Twitter](https://twitter.com/awkwardferny)
